//
//  ViewController.swift
//  Dicee
//
//  Created by Hasan Tagiyev on 8/4/18.
//  Copyright © 2018 Hasan Tagiyev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var randomDiceIndex1: Int = 0
    var randomDiceIndex2: Int = 0
    
    @IBOutlet weak var diceImageView1: UIImageView!
    @IBOutlet weak var diceImageView2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        shakeDices()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func shakeDices() -> Void {
        randomDiceIndex1 = Int(arc4random_uniform(6))
        randomDiceIndex2 = Int(arc4random_uniform(6))
        let text1 = "dice" + String(randomDiceIndex1+1)
        let text2 = "dice" + String(randomDiceIndex2+1)
        diceImageView1.image = UIImage(named: text1)
        diceImageView2.image = UIImage(named: text2)
    }
    @IBAction func onRollBtnPressed(_ sender: UIButton) {
        shakeDices()
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        shakeDices()
    }
}

